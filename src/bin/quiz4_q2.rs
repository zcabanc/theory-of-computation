use theory_of_computation::cfl::*;
use std::collections::HashSet;
use regex;
use std::str::FromStr;
use std::iter::FromIterator;

fn main() {
	let cfg_rules: Rules<char, char> = "\
X->01
X->RXR
X->
R->001
R->".parse().unwrap();

  let hypothesis = regex::Regex::from_str("^(01|(001)*(01)?(001)*)?$").unwrap();

  let mut visited = HashSet::with_capacity(1000000);
	for generated in cfg_rules.generate_strings() {
    let str_generated = String::from_iter(generated.into_iter());
    if visited.contains(&str_generated) {
      continue;
    }
    println!("{}", &str_generated);
    if !hypothesis.is_match(&str_generated) {
      eprintln!("!!! {} not matched by regex!", &str_generated);
      return;
    }
    visited.insert(str_generated);
	}
}
