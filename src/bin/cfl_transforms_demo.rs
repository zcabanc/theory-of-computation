use theory_of_computation::cfl::Rules;
use std::io;
use std::io::Read;
fn main () {
  let mut grammar_str = String::new();
  io::stdin().read_to_string(&mut grammar_str).expect("Read failure");
  let grammar: Rules<char, char> = grammar_str.trim_end().parse().expect("Invalid grammar");
  let growing = grammar.to_growing_grammar();
  print!("Growing: \n{}\n", indent(&format!("{}", &growing)));
}

fn indent(s: &str) -> String {
  let mut r = String::new();
  for l in s.lines() {
    r.push_str("  ");
    r.push_str(l);
    r.push('\n');
  }
  r
}
