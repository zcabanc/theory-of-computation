use theory_of_computation::cfl::*;
use std::iter::FromIterator;

fn main() {
	let cfg_rules: Rules<char, char> = "\
E->T
E->T+E
T->(E)
T->T*T
T->i".parse().unwrap();

  let growing = cfg_rules.to_growing_grammar();

  print!("{}\n\n", &growing);

	for generated in growing.generate_strings() {
    if generated.len() > 20 {
      break;
    }
    println!("{}", String::from_iter(generated.into_iter()));
	}
}
