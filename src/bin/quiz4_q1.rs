use theory_of_computation::cfl::*;
use std::iter::FromIterator;

fn main() {
	let cfg_rules: Rules<char, char> = "\
S->VbS
S->Vbba
S->bbc
V->AA
V->ac
A->Vcc
A->c".parse().unwrap();

  let strs_to_check = &[
    "acbbbc",
    "bcbcabba",
    "aaabbc",
    "cacccbbbc",
    "cacbbacba",
  ];

	for generated in cfg_rules.generate_strings() {
    let str_generated = String::from_iter(generated.into_iter());
    for test in strs_to_check {
      if &str_generated == *test {
        println!("Found {}!", test);
      }
    }
	}
}
