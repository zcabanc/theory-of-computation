//! Context-free language & push-down automaton (TODO) utilities
//!
//! [`Rules<S, Q>`](Rules) expresses a context-free grammar with alphabet S and
//! variables Q. This is a language containing strings of type `[S]`.
//!
//! Syntax for `from_str` of [`Rules<char, char>`](Rules) and [`Rule`](Rule):
//!
//! `A->aBc` means that the variable A expands into terminal 'a', variable 'B' and terminal 'c'.
//!
//! * Variables are always capital letter.
//! * Terminals can be lower-case letter or numbers.
//!
//! For [`Rules`](Rules), the left-side of the first rule is the starting variable.

use std::pin::Pin;
use std::ops::{Generator, GeneratorState, Deref, DerefMut};
use std::str::FromStr;
use std::fmt;
use std::fmt::Display;
use std::collections::{VecDeque, HashMap, HashSet};
use std::iter::FromIterator;
use std::hash::Hash;

/// Either a variable or a terminal character
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Item<S: Eq + Copy, Q: Eq + Copy> {
  Var(Q),
  Terminal(S)
}

/// A single replacement rule.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Rule<S: Eq + Copy, Q: Eq + Copy> {
  pub from: Q,
  pub to: Vec<Item<S, Q>>,
}

fn try_all_terminals_to_str<S: Eq + Copy, Q: Eq + Copy>(a: &Vec<Item<S, Q>>) -> Option<Vec<S>> {
	let mut res = Vec::new();
	for i in a.iter() {
		match *i {
			Item::Var(_) => return None,
			Item::Terminal(c) => res.push(c)
		};
	}
	assert_eq!(res.len(), a.len());
	Some(res)
}

fn vec_item_is_growing<S: Eq + Copy, Q: Eq + Copy>(ii: &[Item<S, Q>]) -> bool {
	let mut found_terminal = false;
	let mut nb_vars = 0;
	for i in ii {
		match i {
			Item::Terminal(_) => {
				found_terminal = true;
				break;
			},
			Item::Var(_) => {
				nb_vars += 1;
				if nb_vars >= 2 {
					break;
				}
			},
		}
	}
	found_terminal || nb_vars >= 2
}

impl<S: Eq + Copy, Q: Eq + Copy> Rule<S, Q> {
	pub fn new(from: Q, to: Vec<Item<S, Q>>) -> Self {
		Rule{from, to}
	}

	pub fn is_growing(&self) -> bool {
		vec_item_is_growing(&self.to)
	}
}

impl FromStr for Rule<char, char> {
	type Err = String;

	/// Parse something like `"S->aSb"` into a context-free rule.
	fn from_str(s: &str) -> Result<Self, Self::Err> {
		if s.len() == 0 {
			return Err("nothing to parse?".to_owned());
		}
		let mut iter = s.chars();
		let var_name = iter.next().unwrap();
		if !var_name.is_ascii_uppercase() {
			return Err(format!("variable name {:?} must be upper case.", var_name));
		}
		let from = var_name;
		let mut to = Vec::new();
		if iter.next().unwrap_or(' ') != '-' || iter.next().unwrap_or(' ') != '>' {
			return Err(format!("expected \"->\" following variable name {:?}", var_name));
		}
		for c in iter {
			if c.is_ascii_uppercase() {
				to.push(Item::Var(c));
			} else if simple_terminal_char(c) {
				to.push(Item::Terminal(c));
			} else {
				return Err(format!("Unexpected character {}", c));
			}
		}
		Ok(Rule::new(from, to))
	}
}

fn simple_terminal_char(c: char) -> bool {
	c.is_ascii_alphanumeric() || "+-*/()".find(c).is_some()
}

impl<Q: Variable + Display> Display for Rule<char, Q> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}->", self.from)?;
		for i in self.to.iter() {
			match i {
				Item::Var(v) => write!(f, "{}", v)?,
				Item::Terminal(c) => {
					if simple_terminal_char(*c) {
						write!(f, "{}", c)?;
					} else {
						write!(f, "{:?}", c)?;
					}
				}
			}
		}
		Ok(())
	}
}

pub trait Alphabet = Eq + Copy + Hash + fmt::Debug;
pub trait Variable = Eq + Copy + Hash + fmt::Debug;

/// Represents a set of context-free grammar rules.
#[derive(Debug, Clone, Eq)]
pub struct Rules<S: Alphabet, Q: Variable> (pub Vec<Rule<S, Q>>);

impl<S: Alphabet, Q: Variable> Rules<S, Q> {
	/// Test if two rules have the exact same rules (order insensitive).
	///
	/// This does not test whether they express the same language, since there are
	/// more than one ways of expressing a language.
	///
	/// Whether two CFGs expresses the same language is, in general, undeciable.
	pub fn literally_the_same_as(&self, other: &Self) -> bool {
		if self.len() != other.len() {
			return false;
		}
		HashSet::<_, std::collections::hash_map::RandomState>::from_iter(self.iter()) == HashSet::from_iter(other.iter())
	}

	/// Check if this grammar is growing. See
	/// [`to_growing_grammar`](Rules::to_growing_grammar) for more info.
	pub fn is_growing_grammar(&self) -> bool {
		if self.len() == 0 {
			return true;
		}
		let starting_variable = self[0].from;
		let mut starting_variable_contained_itself = false;
		let mut starting_variable_empty = false;
		for rule in self.iter() {
			if rule.from == starting_variable {
				starting_variable_contained_itself |= rule.to.iter().any(|i| i == &Item::Var(starting_variable));
			}
			if !rule.is_growing() {
				if rule.to.len() == 0 && rule.from == starting_variable {
					starting_variable_empty |= true;
				} else {
					return false;
				}
			}
		}
		if starting_variable_contained_itself {
			!starting_variable_empty
		} else {
			true
		}
	}

	pub fn group_by_variables<'a>(&'a self) -> HashMap<Q, Vec<&'a Vec<Item<S, Q>>>> {
		let mut var_rules = HashMap::with_capacity(self.len());
		for r in self.iter() {
			var_rules.entry(r.from).or_insert(Vec::new()).push(&r.to);
		}
		var_rules
	}

	pub fn remove_unreachables(&mut self) {
		if self.len() == 0 {
			return;
		}
		let mut reached_variables = HashSet::new();
		let starting_var = self[0].from;
		reached_variables.insert(starting_var);
		let mut queue = VecDeque::new();
		queue.push_back(starting_var);
		let var_rules = self.group_by_variables();
		while !queue.is_empty() {
			let v = queue.pop_front().unwrap();
			debug_assert!(reached_variables.contains(&v));
			for r in var_rules.get(&v).unwrap() {
				for i in r.iter() {
					if let Item::Var(v) = *i {
						if !reached_variables.contains(&v) {
							if var_rules.contains_key(&v) {
								reached_variables.insert(v);
								queue.push_back(v);
							}
						}
					}
				}
			}
		}
		self.retain(|r| {
			if !reached_variables.contains(&r.from) {
				return false;
			}
			for i in r.to.iter() {
				if let Item::Var(v) = i {
					if !reached_variables.contains(&v) {
						return false;
					}
				}
			}
			true
		});
	}
}

#[derive(PartialEq, Eq, Copy, Clone, Hash, Debug)]
pub enum IsolatedStartQ<Q: Variable> {
	Original(Q),
	IsolatedStart
}

impl<S: Alphabet, Q: Variable> Rules<S, Q> {
	/// Turn this grammar into another grammar that accepts the same language but
	/// is "growing", which means that any rules either:
	///
	/// * Expands into at least one terminal, or...
	/// * Expands into at least two variables
	/// * ...unless it is the starting rule, in which case it is allowed to expand into the empty string, but...
	/// * if any starting rule expands into the empty string, the starting variable must not appear in the expansion of any rules.
	pub fn to_growing_grammar(&self) -> Rules<S, IsolatedStartQ<Q>> {
		if self.len() == 0 {
			return Rules::new();
		}
		let starting_var = self[0].from;
		let mut removed_empty_str = false;
		let mut current_iter = self.clone();

		while !current_iter.is_growing_grammar() {
			// Get rid of A->(empty) rules.
			let mut empty_var_removed = HashSet::new();
			let filtered_rules: Vec<_> = current_iter.iter().filter(|r| {
				if r.to.len() == 0 {
					empty_var_removed.insert(r.from);
					false
				} else if r.to == vec![Item::Var(r.from)] {
					false
				} else {
					true
				}
			}).collect();
			if empty_var_removed.contains(&starting_var) {
				removed_empty_str = true;
			}
			let mut new_rules = HashSet::new();
			for r in filtered_rules.into_iter() {
				fn process<S: Alphabet, Q: Variable>(empty_var_removed: &HashSet<Q>, f: Q, r: &[Item<S, Q>], mut p: Vec<Item<S, Q>>, b: &mut HashSet<Rule<S, Q>>) {
					if r.len() == 0 {
						b.insert(Rule::new(f, p));
						return;
					}
					let fst = &r[0];
					if let Item::Var(v) = *fst {
						if !empty_var_removed.contains(&v) {
							p.push(*fst);
							process(empty_var_removed, f, &r[1..], p, b);
						} else {
							p.push(*fst);
							process(empty_var_removed, f, &r[1..], p.clone(), b);
							p.pop().unwrap();
							process(empty_var_removed, f, &r[1..], p, b);
						}
					} else {
						p.push(*fst);
						process(empty_var_removed, f, &r[1..], p, b);
					}
				}
				process(&empty_var_removed, r.from, &r.to, Vec::new(), &mut new_rules);
			}

			use std::cell::RefCell;
			let mut var_rules = HashMap::new();
			for r in new_rules.into_iter() {
				var_rules.entry(r.from).or_insert(RefCell::new(Vec::new())).borrow_mut().push(r.to);
			}

			// Fully expand all A->B rules by doing bfs on each A->B rule.
			let mut expanded_vars = HashSet::new();
			for (var, expans) in var_rules.iter() {
				expanded_vars.clear();
				expanded_vars.insert(*var);
				loop {
					let mut new_expands = Vec::new();
					expans.borrow_mut().retain(|exp| {
						if exp.len() == 1 {
							if let Item::Var(v_to) = exp[0] {
								if !expanded_vars.contains(&v_to) {
									expanded_vars.insert(v_to);
									assert!(v_to != *var);
									if let Some(expands_to) = var_rules.get(&v_to) {
										let expands_to = expands_to.borrow();
										new_expands.push(expands_to);
									}
								}
								return false;
							}
						}
						true
					});
					if new_expands.len() == 0 {
						break;
					}
					let mut expans = expans.borrow_mut();
					for expansions_to_add in new_expands.into_iter() {
						expans.extend_from_slice(&expansions_to_add[..]);
					}
				}
			}

			let mut new_rules = HashSet::new();
			for r in var_rules.into_iter().flat_map(|(var, expansions)| expansions.into_inner().into_iter().map(move |e| Rule::new(var, e.clone()))) {
				new_rules.insert(r);
			}
			current_iter = Rules(new_rules.into_iter().collect());
		}

		if current_iter.iter().all(|r| {
			r.to.iter().any(|i| if let Item::Var(_) = i { true } else { false })
		}) {
			current_iter = Rules::new(); // Never-terminating grammar produces no strings.
		}

		fn map_var<S: Alphabet, Q: Variable>(i: Item<S, Q>) -> Item<S, IsolatedStartQ<Q>> {
			match i {
				Item::Var(v) => Item::Var(IsolatedStartQ::Original(v)),
				Item::Terminal(t) => Item::Terminal(t)
			}
		}

		if !removed_empty_str {
			if current_iter.len() > 0 && current_iter[0].from != starting_var {
				let swap_with = current_iter.iter().position(|r| r.from == starting_var).unwrap();
				current_iter.swap(0, swap_with);
			}
			current_iter.remove_unreachables();
			return Rules(current_iter.0.into_iter().map(|r| {
				Rule::new(IsolatedStartQ::Original(r.from), r.to.into_iter().map(map_var).collect())
			}).collect());
		} else {
			let start_expansions = current_iter.iter().filter(|r| r.from == starting_var);
			let mut result = Rules::new();
			result.push(Rule::new(IsolatedStartQ::IsolatedStart, Vec::new())); // For S->
			for e in start_expansions {
				debug_assert_eq!(e.from, starting_var);
				result.push(Rule::new(IsolatedStartQ::IsolatedStart, e.to.iter().map(|x| *x).map(map_var).collect()));
			}
			for r in current_iter.0.into_iter() {
				result.push(Rule::new(IsolatedStartQ::Original(r.from), r.to.into_iter().map(map_var).collect()));
			}
			result.remove_unreachables();
			result
		}
	}
}

impl Display for IsolatedStartQ<char> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			IsolatedStartQ::Original(v) => write!(f, "{}", v),
			IsolatedStartQ::IsolatedStart => write!(f, "(_start)")
		}
	}
}

impl<S: Alphabet, Q: Variable> PartialEq for Rules<S, Q> {
	/// Alias for [`literally_the_same_as`](Rules::literally_the_same_as).
	fn eq(&self, other: &Self) -> bool {
		self.literally_the_same_as(other)
	}
}

/// The iterator producing strings of the language described by some [`Rules`](Rules)
pub struct GenerateString<S, G: Generator<Yield = Vec<S>, Return = ()>> {
	generator: G
}

impl<S: Alphabet, Q: Variable> Rules<S, Q> {
	/// Create a `Rules` that has nothing in it, not even the start variable.
	///
	/// To use it, you need to add rules into it by using `rules.push()` (`Rules` derefs to `Vec<Rule>`).
	pub fn new() -> Self {
		Rules(Vec::new())
	}

	/// Returns an iterator producing strings in this language.
	///
	/// If the language is ambiguous, duplicate strings can be produced by this iterator.
  pub fn generate_strings<'a>(&'a self) -> GenerateString<S, impl Generator<Yield = Vec<S>, Return = ()> + 'a> {
    let generator = move || {
			if self.is_empty() {
				return;
			}
			let var_rules = self.group_by_variables();
			let mut dfs_queue = VecDeque::new();
			let starting_var = self.first().unwrap().from;
			dfs_queue.push_back(vec![Item::Var(starting_var)]);
			loop {
				if dfs_queue.is_empty() {
					return;
				}
				let to_expand = dfs_queue.pop_front().unwrap();
				if let Some(s) = try_all_terminals_to_str(&to_expand) {
					yield s;
				} else {
					fn expand_rest<S: Alphabet, Q: Variable>(var_rules: &HashMap<Q, Vec<&Vec<Item<S, Q>>>>, dfs_queue: &mut VecDeque<Vec<Item<S, Q>>>, rest: &[Item<S, Q>], result_prefix: &mut Vec<Item<S, Q>>) {
						if rest.is_empty() {
							dfs_queue.push_back(result_prefix.clone());
							return;
						}
						let mut first_var_idx: Option<usize> = None;
						let prefix_len = result_prefix.len();
						for (i, it) in rest.iter().enumerate() {
							match *it {
								Item::Terminal(c) => result_prefix.push(Item::Terminal(c)),
								Item::Var(_) => {
									first_var_idx = Some(i);
									break;
								}
							}
						}
						if let Some(first_var_idx) = first_var_idx {
							let first_var_it = rest[first_var_idx];
							let first_var = match first_var_it {
								Item::Var(v) => v,
								_ => unreachable!()
							};
							for var_replace_with in var_rules.get(&first_var).unwrap_or(&Vec::new()) {
								let preifx_len_before_extend = result_prefix.len();
								result_prefix.extend_from_slice(var_replace_with);
								expand_rest(var_rules, dfs_queue, &rest[first_var_idx+1..], result_prefix);
								result_prefix.truncate(preifx_len_before_extend);
							}
						} else {
							expand_rest(var_rules, dfs_queue, &[], result_prefix);
						}
						result_prefix.truncate(prefix_len);
					};
					expand_rest(&var_rules, &mut dfs_queue, &to_expand, &mut Vec::new());
				}
			}
    };
		GenerateString::<S, _>{generator}
  }
}

impl<S, G: Generator<Yield = Vec<S>, Return = ()> + std::marker::Unpin> Iterator for GenerateString<S, G> {
	type Item = Vec<S>;
	fn next(&mut self) -> Option<Vec<S>> {
		match Pin::new(&mut self.generator).resume(()) {
			GeneratorState::Yielded(s) => Some(s),
			GeneratorState::Complete(_) => None
		}
	}
}

impl<S: Alphabet, Q: Variable> Deref for Rules<S, Q> {
	type Target = Vec<Rule<S, Q>>;
	fn deref(&self) -> &Vec<Rule<S, Q>> {
		&self.0
	}
}
impl<S: Alphabet, Q: Variable> DerefMut for Rules<S, Q> {
	fn deref_mut(&mut self) -> &mut Vec<Rule<S, Q>> {
		&mut self.0
	}
}

impl<S: Alphabet, Q: Variable> FromStr for Rules<S, Q> where Rule<S, Q>: FromStr, <Rule<S, Q> as std::str::FromStr>::Err: Display {
	type Err = String;

	/// Parse a context-free grammar.
	///
	/// ## Example:
	///
	/// ```
	/// use theory_of_computation::cfl::Rules;
	/// let r: Rules<char, char> = "\
	/// S->
	/// S->aSb".parse().unwrap();
	/// ```
	fn from_str(s: &str) -> Result<Self, Self::Err> {
		let mut rules = Rules::new();
		if s.len() == 0 {
			return Ok(rules);
		}
		for (i, line) in s.split('\n').enumerate() {
			rules.push(match Rule::from_str(line) {
				Ok(r) => r,
				Err(e) => return Err(format!("Error parsing rule {}: {}", i + 1, &e))
			});
		}
		Ok(rules)
	}
}

impl<S: Alphabet, Q: Variable> Display for Rules<S, Q> where Rule<S, Q>: Display {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		for rule in self.iter() {
			write!(f, "{}\n", &rule)?;
		}
		Ok(())
	}
}
