use theory_of_computation::cfl;
use cfl::{Item, Rules};

fn expect_fault(grammar: &str) {
  grammar.parse::<cfl::Rules<char, char>>().expect_err("Did not error");
}

fn expect_parse(grammar: &str, expect_rules: &cfl::Rules<char, char>) {
  assert_eq!(&grammar.parse::<cfl::Rules<char, char>>().expect("Parse error"), expect_rules);
}

#[test]
fn test_parse() {
  expect_parse("S->", &Rules(vec![cfl::Rule::new('S', vec![])]));
  expect_parse("S->s", &Rules(vec![cfl::Rule::new('S', vec![Item::Terminal('s')])]));
  expect_parse("", &Rules(vec![]));
  expect_fault("S");
  expect_fault("s->");
  expect_fault("S->!");
  expect_fault("!->s");
  expect_parse("S->A", &Rules(vec![cfl::Rule::new('S', vec![Item::Var('A')])]));
  expect_parse("S->0ab1", &Rules(vec![cfl::Rule::new('S', vec![Item::Terminal('0'), Item::Terminal('a'), Item::Terminal('b'), Item::Terminal('1')])]));
  expect_parse("S->AaA",
    &Rules(vec![
      cfl::Rule::new('S', vec![Item::Var('A'), Item::Terminal('a'), Item::Var('A')])
    ])
  );
  expect_parse("A->B\nB->Aa",
    &Rules(vec![
      cfl::Rule::new('A', vec![Item::Var('B')]),
      cfl::Rule::new('B', vec![Item::Var('A'), Item::Terminal('a')])
    ])
  );
}
