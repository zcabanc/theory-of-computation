use theory_of_computation::cfl;
use std::collections::HashSet;
use std::iter::FromIterator;

fn test_cfl_generate(expected: Vec<&str>, rules: &str, only_test_growing: bool) {
  let g: cfl::Rules<char, char> = rules.parse().unwrap();
  let expected_strings = HashSet::from_iter(expected.into_iter());
  let max_len = expected_strings.iter().max_by_key(|x| x.len()).unwrap_or(&"").len();
  let mut generated_strings = HashSet::new();
  if !only_test_growing {
    for s in g.generate_strings() {
      if s.len() > max_len*3 {
        break;
      }
      if s.len() <= max_len {
        generated_strings.insert(Box::leak(Box::new(String::from_iter(s.into_iter()))).as_str());
      }
    }
    assert_eq!(generated_strings, expected_strings);
  }
  let g = g.to_growing_grammar();
  generated_strings.clear();
  for s in g.generate_strings() {
    if s.len() > max_len*3 {
      break;
    }
    if s.len() <= max_len {
      generated_strings.insert(Box::leak(Box::new(String::from_iter(s.into_iter()))).as_str());
    }
  }
  assert_eq!(generated_strings, expected_strings);
}

#[test]
fn test_empty() {
  test_cfl_generate(vec![], "", false);
  test_cfl_generate(vec![], "S->S", true);
  test_cfl_generate(vec![], "\
S->A
A->B
B->C
C->S", true);
  test_cfl_generate(vec![], "\
S->A
A->B
B->C
C->A", true);
  test_cfl_generate(vec![], "\
S->A
A->B
B->C
C->aA", true);
  test_cfl_generate(vec![], "S->aSb", true);
  test_cfl_generate(vec![], "S->aSb\nC->", true);
  test_cfl_generate(vec![], "A->AA", true);
  test_cfl_generate(vec![], "A->AAAAAAAAAA", true);
  test_cfl_generate(vec![], "A->BB\nB->A", true);
  test_cfl_generate(vec![], "A->BB\nB->B", true);
  test_cfl_generate(vec![], "A->BB\nB->BaB", true);
  test_cfl_generate(vec![], "A->BB\nB->bAb", true);
}

#[test]
fn test_basic() {
  test_cfl_generate(vec![""], "S->", false);
  test_cfl_generate(vec![""], "\
S->
A->", false);
  test_cfl_generate(vec!["a"], "\
S->Aa
A->B
B->", false);
  test_cfl_generate(vec!["", "a", "aa", "aaa", "aaaa", "aaaaa", "aaaaaa"], "\
S->A
A->
A->aA", false);
  test_cfl_generate(vec!["1", "2", "3"], "\
A->1
A->B
B->2
B->C
C->3
C->A", true);
}

#[test]
fn test_anbn() {
  test_cfl_generate(vec![
    "",
    "ab",
    "aabb",
    "aaabbb",
    "aaaabbbb",
    "aaaaabbbbb",
    "aaaaaabbbbbb",
    "aaaaaaabbbbbbb",
    "aaaaaaaabbbbbbbb",
    "aaaaaaaaabbbbbbbbb",
    "aaaaaaaaaabbbbbbbbbb",
    "aaaaaaaaaaabbbbbbbbbbb",
    "aaaaaaaaaaaabbbbbbbbbbbb",
    "aaaaaaaaaaaaabbbbbbbbbbbbb",
  ], "\
S->
S->ab
S->aSb", false);
}

#[test]
fn test_pal() {
  test_cfl_generate(vec![
    "",
    "a",
    "b",
    "aa",
    "bb",
    "aaa",
    "bbb",
    "aba",
    "bab",
    "aaaa",
    "abba",
    "baab",
    "bbbb",
    "aaaaa",
    "aabaa",
    "ababa",
    "abbba",
    "baaab",
    "babab",
    "bbabb",
    "bbbbb",
  ], "\
S->
S->a
S->b
S->aSa
S->bSb", false);
}

#[test]
fn test_pal_nonempty() {
  test_cfl_generate(vec![
    "a",
    "b",
    "aa",
    "bb",
    "aaa",
    "bbb",
    "aba",
    "bab",
    "aaaa",
    "abba",
    "baab",
    "bbbb",
    "aaaaa",
    "aabaa",
    "ababa",
    "abbba",
    "baaab",
    "babab",
    "bbabb",
    "bbbbb",
  ], "\
S->a
S->b
S->aSa
S->bSb
S->aa
S->bb", false);
}

#[test]
fn test_looping() {
  test_cfl_generate(vec![""], "\
S->A
A->B
B->C
B->
C->A", true);
  test_cfl_generate(vec![], "\
S->A
A->1A
A->B
B->2B
B->C
C->3C
C->A", true);
  test_cfl_generate(vec!["", "1", "2", "3", "11", "12", "13", "21", "22", "23", "31", "32", "33"], "\
S->A
A->1A
A->B
B->2B
B->C
C->3C
C->A
C->", false);
  test_cfl_generate(vec!["", "1", "2", "3", "11", "12", "13", "22", "23", "33", "111", "112", "113", "122", "123", "133", "222", "223", "233", "333"], "\
S->A
A->1A
A->B
B->2B
B->C
C->3C
C->", false);
}
